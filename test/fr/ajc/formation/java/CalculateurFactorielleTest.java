package fr.ajc.formation.java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurFactorielleTest {

	@Test
	void leresultatdefactoriellede5est120() {
		assertEquals(120, CalculateurFactorielle.factorielle(5));
	}
	
		
	@Test
	void leresultatdefactoriellede6est720() {
		assertEquals(720, CalculateurFactorielle.factorielle(6));
	}
}
